def nodeName = env.NODE_NAME

node {
    properties([
            parameters([string(name: 'APP_NAME', defaultValue: '', description: 'Application name'),
                        string(name: 'REPOSITORY', defaultValue: '', description: 'Choose source code repository to build'),
                        string(name: 'BRANCH', defaultValue: '', description: 'Choose branch'),
                        string(name: 'DOCKERFILE_LOCATION', defaultValue: '.', description: 'Choose directory of Dockerfile'),
                        [$class: 'ChoiceParameter',
                            choiceType: 'PT_CHECKBOX',
                            description: 'Select a choice',
                            filterLength: 1,
                            filterable: false,
                            name: 'Deploy',
                            script: [
                                 $class: 'GroovyScript',
                                 script: [
                                         classpath: [],
                                         sandbox: false,
                                         script: """
                                                 return['Deploy']
                                                 """
                                                 ]]]
                                     ])])
    def deploy = "${params.Deploy}"

    library(identifier: 'my-library@main', retriever: legacySCM(scm))

    stage('Source Code Download') {
        git([url: "${env.REPOSITORY}",
             branch: "${env.BRANCH}"])
    }
    stage('Build Docker') {
        def newApp = docker.build("${env.APP_NAME}:${env.BUILD_ID}")
    }

    stage('Deploy') {
        if (env.Deploy.isEmpty()) {
            println("Deploy is not selected")
        }
        else {
            println("Deploy is selected")
            loadDeployParams()
        }
    }
    cleanWs()
}