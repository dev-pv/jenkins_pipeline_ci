#!/usr/bin/env groovy

def call(){
    def props = libraryResource('deploy_config.json')
    def jsonData = readJSON(text: props)
    jsonData.each { k, v ->
        println(k + " " + v)
    }
}